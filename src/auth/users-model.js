'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Users = new mongoose.Schema({
  username: {type:String, required:true},
  password: {type:String, required:true},
  email: {type:String},
  role: {type:String, required:true, default:'user', enum:['user', 'admin']},
});

Users.pre('save', function(next){
  bcrypt.hash(this.password, 10)
    .then(hashedPassword => {
      this.password = hashedPassword;
      next();
    })
    .catch(error => {throw error;});
});

Users.statics.authenticateToken = function(token){
  let parsedToken = jwt.verify(token, process.env.SECRET);
  let query = {_id: parsedToken.id};
  return this.findOne(query);
};

Users.statics.authenticateBasic = function(auth){
  let query = {username:auth.username};
  return this.findOne(query)
    .then(user => user && user.comparePassword(auth.password))
    .catch(error => {throw error;});
};

Users.statics.createFromOauth = function(oauthUser){
  if(!oauthUser) { return Promise.reject('Validation Error'); }

  let email = oauthUser.email;

  //TODO: Probably needs to be better
  return this.findOne({email})
    .then(user => {
      if(!user) {throw new Error('User Not Found'); }
      return user;
    })
    .catch(error => {
      let username = email;
      let password = 'none';
      return this.create({username, password, email});
    });
};

Users.methods.comparePassword = function(password){
  return bcrypt.compare(password, this.password)
    .then(valid => valid ? this : null);
};

Users.methods.generateToken = function(){
  let tokenData = {
    id: this._id,
    role: this.role,
  };

  return jwt.sign(tokenData, process.env.SECRET, {expiresIn: '10m'});
};

module.exports = mongoose.model('users', Users);
