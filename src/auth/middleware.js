'use strict';

const User = require('./users-model');

module.exports = (req, res, next) => {
  try {
    let [authType, authString] = req.headers.authorization.split(/\s+/);

    switch(authType.toLowerCase()) {
    case 'basic':
      return _authBasic(authString);
    case 'bearer':
      return _authBearer(authString);
    default:
      return _authError();
    }
  } catch(e) {
    return _authError(e);
  }

  function _authBasic(authString) {
    let base64Buffer = Buffer.from(authString,'base64'); // <Buffer 01 02...>
    let bufferString = base64Buffer.toString(); // hlorn:boobz
    let [username,password] = bufferString.split(':');  // variables username="hlorn" and password="boobz"
    let auth = {username,password};  // {username:"hlorn", password:"boobz"}

    return User.authenticateBasic(auth)
      .then( user => _authenticate(user) );
  }

  function _authBearer(authString){
    return User.authenticateToken(authString)
      .then(user => _authenticate(user))
      .catch(next);
  }

  function _authenticate(user) {
    if ( user ) {
      req.user = user;
      req.token = user.generateToken();
      next();
    }
    else {
      _authError({statusMessage: 'Unauthorized', message: 'Invalid User ID/Password'});
    }
  }

  function _authError(e) {
    next({status: 401, error: e});
  }
};