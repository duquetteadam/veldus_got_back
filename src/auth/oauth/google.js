'use strict';

const superagent = require('superagent');
const Users = require('../users-model');

const authorize = (req) => {
  return superagent.post(process.env.GOOGLE_TOKEN_URL)
    .type('form')
    .send({
      code: req.query.code,
      client_id: process.env.GOOGLE_CLIENT_ID,
      client_secret: process.env.GOOGLE_CLIENT_SECRET,
      redirect_uri: `${process.env.API_URL}/oauth`,
      grant_type: 'authorization_code',
    })
    .then(response => response.body.access_token)
    .then(token => {
      return superagent.get(process.env.GOOGLE_OPENIDCONNECT_URL)
        .set('Authorization', `Bearer ${token}`)
        .then(response => {
          let user = response.body;
          user.access_token = token;
          return user;
        });
    })
    .then(oauthUser => Users.createFromOauth(oauthUser))
    .then(actualRealUser => actualRealUser.generateToken())
    .catch(error => error);
};

module.exports = {authorize};
