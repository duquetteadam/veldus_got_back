'use strict';

const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000;

const cors = require('cors');
const morgan = require('morgan');

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:true}));

const notFound = require('./errors/404');
const errorHandler = require('./errors/500');

const authRouter = require('./auth/router');
app.use(authRouter);

const auth = require('./auth/middleware');

app.get('/', (req, res) => {
  res.status(200).send('hello');
});

app.get('/error', auth, (req, res, next) => {
  next('FAKE server error encountered');
});

app.use(notFound);
app.use(errorHandler);

module.exports = {
  server: app,
  start: (port) => {
    app.listen(port || PORT, () => console.log(`Listening on ${port || PORT}`));
  },
};

