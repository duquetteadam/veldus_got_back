# veldus_got_back

### env

![node v12.11.1 yarn v1.19.1 mac os x 10.14.6 build version 18G95](./docs/assets/env.png)

## install

git the code

```git clone git@gitlab.com:duquetteadam/veldus_got_back.git && cd $_```

install packages

```yarn install```

add _.env-dev_ to root

```cp env-sample .env-dev```

update _.env-dev_ with appropriate information

```code .env-dev```

## run

run in debug mode on watch

```yarn run watch```

say hello

```curl http://localhost:3000/```

## auth

secret fake error

```curl http://localhost:3000/error```

we need to sign up (ignore TOKEN)

```curl -d '{"username":"foo", "password":"bar"}' -H "Content-Type: application/json" -X POST http://localhost:3000/signup```

signin (ok save this TOKEN)

```curl -H "Authorization: Basic $(echo -n foo:bar | base64)" http://localhost:3000/signin```

secret fake error 2: revenge of the error

```curl -H "Authorization: Bearer TOKEN" http://localhost:3000/error```

whata beaut'!
