'use strict';

// I am not smart, probably better way to do this
let env = {path: '.env'};
if(process.env.NODE_ENV !== 'production'){
  env.path = '.env-dev';
}

require('dotenv').config(env);

const mongoose = require('mongoose');

const mongooseOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
};

mongoose.connect(process.env.MONGODB_URI, mongooseOptions);

require('./src/server.js').start();
